from django.conf.urls import url 
from .views import *


urlpatterns = [
    url(r'^school-admin/logout/$', AdminLogout.as_view(),
        name='adminlogout'),
    url(r'^school-admin/login/$', AdminLogin.as_view(),
        name='adminlogin'),
    url(r'^school-admin/$', AdminHomeView.as_view(), name='adminhome'),



    url(r'^school-admin/videos/$',
        AdminVideoListView.as_view(), name='adminvideolist'),
    url(r'^school-admin/videos/create/$',
        AdminVideoCreateView.as_view(), name='adminvideocreate'),
    url(r'^school-admin/videos/(?P<pk>\d+)/update/$',
        AdminVideoUpdateView.as_view(), name='adminvideoupdate'),
    url(r'^school-admin/videos/(?P<pk>\d+)/delete/$',
        AdminVideoDeleteView.as_view(), name='adminvideodelete'),



    url(r'^school-admin/blogs/$',
        AdminBlogListView.as_view(), name='adminbloglist'),
    url(r'^school-admin/blogs/create/$',
        AdminBlogCreateView.as_view(), name='adminblogcreate'),
    url(r'^school-admin/blogs/(?P<pk>\d+)/update/$',
        AdminBlogUpdateView.as_view(), name='adminblogupdate'),
    url(r'^school-admin/blogs/(?P<pk>\d+)/delete/$',
        AdminBlogDeleteView.as_view(), name='adminblogdelete'),

    url(r'^school-admin/events/$',
        AdminEventListView.as_view(), name='admineventlist'),
    url(r'^school-admin/events/create/$',
        AdminEventCreateView.as_view(), name='admineventcreate'),
    url(r'^school-admin/events/(?P<pk>\d+)/update/$',
        AdminEventUpdateView.as_view(), name='admineventupdate'),
    url(r'^school-admin/events/(?P<pk>\d+)/delete/$',
        AdminEventDeleteView.as_view(), name='admineventdelete'),

    url(r'^school-admin/gallery/$',
        AdminGalleryListView.as_view(), name='admingallerylist'),
    url(r'^school-admin/gallery/create/$',
        AdminGalleryCreateView.as_view(), name='admingallerycreate'),
    url(r'^school-admin/gallery/(?P<pk>\d+)/update/$',
        AdminGalleryUpdateView.as_view(), name='admingalleryupdate'),
    url(r'^school-admin/gallery/(?P<pk>\d+)/delete/$',
        AdminGalleryDeleteView.as_view(), name='admingallerydelete'),

    url(r'^school-admin/notices/$',
        AdminNoticeListView.as_view(), name='adminnoticelist'),
    url(r'^school-admin/notices/create/$',
        AdminNoticeCreateView.as_view(), name='adminnoticecreate'),
    url(r'^school-admin/notices/(?P<pk>\d+)/update/$',
        AdminNoticeUpdateView.as_view(), name='adminnoticeupdate'),
    url(r'^school-admin/notices/(?P<pk>\d+)/delete/$',
        AdminNoticeDeleteView.as_view(), name='adminnoticedelete'),

    url(r'^school-admin/staffs/$',
        AdminStaffListView.as_view(), name='adminstafflist'),
    url(r'^school-admin/staffs/create/$',
        AdminStaffCreateView.as_view(), name='adminstaffcreate'),
    url(r'^school-admin/staffs/(?P<pk>\d+)/update/$',
        AdminStaffUpdateView.as_view(), name='adminstaffupdate'),
    url(r'^school-admin/staffs/(?P<pk>\d+)/delete/$',
        AdminStaffDeleteView.as_view(), name='adminstaffdelete'),

    url(r'^school-admin/testimonials/$',
        AdminTestimonialListView.as_view(), name='admintestimoniallist'),
    url(r'^school-admin/testimonials/create/$',
        AdminTestimonialCreateView.as_view(), name='admintestimonialcreate'),
    url(r'^school-admin/testimonials/(?P<pk>\d+)/update/$',
        AdminTestimonialUpdateView.as_view(), name='admintestimonialupdate'),
    url(r'^school-admin/testimonials/(?P<pk>\d+)/delete/$',
        AdminTestimonialDeleteView.as_view(), name='admintestimonialdelete'),


    url(r'^school-admin/activities/$',
        AdminActivitiesListView.as_view(), name='adminactivitieslist'),
    url(r'^school-admin/activities/create/$',
        AdminActivitiesCreateView.as_view(), name='adminactivitiescreate'),
    url(r'^school-admin/activities/(?P<pk>\d+)/update/$',
        AdminActivitiesUpdateView.as_view(), name='adminactivitiesupdate'),
    url(r'^school-admin/activities/(?P<pk>\d+)/delete/$',
        AdminActivitiesDeleteView.as_view(), name='adminactivitiesdelete'),



    url(r'^school-admin/features/$',
        AdminFeaturesListView.as_view(), name='adminfeatureslist'),
    url(r'^school-admin/features/create/$',
        AdminFeaturesCreateView.as_view(), name='adminfeaturescreate'),
    url(r'^school-admin/features/(?P<pk>\d+)/update/$',
        AdminFeaturesUpdateView.as_view(), name='adminfeaturesupdate'),
    url(r'^school-admin/features/(?P<pk>\d+)/delete/$',
        AdminFeaturesDeleteView.as_view(), name='adminfeaturesdelete'),




    url(r'^school-admin/services/$',
        AdminServicesListView.as_view(), name='adminserviceslist'),
    url(r'^school-admin/services/create/$',
        AdminServicesCreateView.as_view(), name='adminservicescreate'),
    url(r'^school-admin/services/(?P<pk>\d+)/update/$',
        AdminServicesUpdateView.as_view(), name='adminservicesupdate'),
    url(r'^school-admin/services/(?P<pk>\d+)/delete/$',
        AdminServicesDeleteView.as_view(), name='adminservicesdelete'),



    url(r'^school-admin/download/$',
        AdminDownloadListView.as_view(), name='admindownloadlist'),
    url(r'^school-admin/download/create/$',
        AdminDownloadCreateView.as_view(), name='admindownloadcreate'),
    url(r'^school-admin/download/(?P<pk>\d+)/update/$',
        AdminDownloadUpdateView.as_view(), name='admindownloadupdate'),
    url(r'^school-admin/download/(?P<pk>\d+)/delete/$',
        AdminDownloadDeleteView.as_view(), name='admindownloaddelete'),



    url(r'^school-admin/routine/$',
        AdminRoutineListView.as_view(), name='adminroutinelist'),
    url(r'^school-admin/routine/create/$',
        AdminRoutineCreateView.as_view(), name='adminroutinecreate'),
    url(r'^school-admin/routine/(?P<pk>\d+)/update/$',
        AdminRoutineUpdateView.as_view(), name='adminroutineupdate'),
    url(r'^school-admin/routine/(?P<pk>\d+)/delete/$',
        AdminRoutineDeleteView.as_view(), name='adminroutinedelete'),




    url(r'^school-admin/result/$',
        AdminResultListView.as_view(), name='adminresultlist'),
    url(r'^school-admin/result/create/$',
        AdminResultCreateView.as_view(), name='adminresultcreate'),
    url(r'^school-admin/result/(?P<pk>\d+)/update/$',
        AdminResultUpdateView.as_view(), name='adminresultupdate'),
    url(r'^school-admin/result/(?P<pk>\d+)/delete/$',
        AdminResultDeleteView.as_view(), name='adminresultdelete'),



    url(r'^school-admin/slider/$',
        AdminSliderListView.as_view(), name='adminsliderlist'),
    url(r'^school-admin/slider/create/$',
        AdminSliderCreateView.as_view(), name='adminslidercreate'),
    url(r'^school-admin/slider/(?P<pk>\d+)/update/$',
        AdminSliderUpdateView.as_view(), name='adminsliderupdate'),
    url(r'^school-admin/slider/(?P<pk>\d+)/delete/$',
        AdminSliderDeleteView.as_view(), name='adminsliderdelete'), 



    url(r'^school-admin/vacancii/$',
        AdminVacanciiListView.as_view(), name='adminvacanciilist'),
    url(r'^school-admin/vacancii/create/$',
        AdminVacanciiCreateView.as_view(), name='adminvacanciicreate'),
    url(r'^school-admin/vacancii/(?P<pk>\d+)/update/$',
        AdminVacanciiUpdateView.as_view(), name='adminvacanciiupdate'),
    url(r'^school-admin/vacancii/(?P<pk>\d+)/delete/$',
        AdminVacanciiDeleteView.as_view(), name='adminvacanciidelete'),



    url(r'^school-admin/price/$',
        AdminPriceListView.as_view(), name='adminpricelist'),
    url(r'^school-admin/price/create/$',
        AdminPriceCreateView.as_view(), name='adminpricecreate'),
    url(r'^school-admin/price/(?P<pk>\d+)/update/$',
        AdminPriceUpdateView.as_view(), name='adminpriceupdate'),
    url(r'^school-admin/price/(?P<pk>\d+)/delete/$',
        AdminPriceDeleteView.as_view(), name='adminpricedelete'),
  




    url(r'^school-admin/messages/$',
        AdminMessageListView.as_view(), name='adminmessagelist'),
    url(r'^school-admin/messages/(?P<pk>\d+)/$',
        AdminMessageDetailView.as_view(), name='adminmessagedetail'),
    url(r'^school-admin/messages/(?P<pk>\d+)/delete/$',
        AdminMessageDeleteView.as_view(), name='adminmessagedelete'),



    url(r'^school-admin/admission/$',
        AdminAdmissionListView.as_view(), name='adminadmissionlist'),
    url(r'^school-admin/admission/(?P<pk>\d+)/$',
        AdminAdmissionDetailView.as_view(), name='adminadmissiondetail'),
    url(r'^school-admin/admission/(?P<pk>\d+)/delete/$',
        AdminAdmissionDeleteView.as_view(), name='adminadmissiondelete'),


    url(r'^school-admin/vacancy/$',
        AdminVacancyListView.as_view(), name='adminvacancylist'),
    url(r'^school-admin/vacancy/(?P<pk>\d+)/$',
        AdminVacancyDetailView.as_view(), name='adminvacancydetail'),
    url(r'^school-admin/vacancy/(?P<pk>\d+)/delete/$',
        AdminVacancyDeleteView.as_view(), name='adminvacancydelete'),

     







    # client side


    url(r'^$', ClientHomeView.as_view(), name='clienthome'),
	url(r'^home$', HomeView.as_view(), name='home'),
	url(r'^about/$', ClientAboutView.as_view(), name='clientabout'),
    url(r'^service/$', ClientServiceListView.as_view(), name='clientservicelist'),
	url(r'^gallery/$', ClientGalleryListView.as_view(), name='clientgallerylist'),
	url(r'^blog/$', ClientBlogListView.as_view(), name='clientbloglist'),
    url(r'^blogs/(?P<pk>\d+)/$', ClientBlogDetailView.as_view(), name='clientblogdetail'),
	url(r'^activities/$', ClientActivitiesListView.as_view(), name='clientactivitieslist'),
    url(r'^activiti/(?P<pk>\d+)/$', ClientActivitiesDetailView.as_view(), name='clientactivitiesdetail'),
    url(r'^contact/$', ClientContactView.as_view(), name='clientcontact'),
    url(r'^events/(?P<pk>\d+)/$', ClientEventDetailView.as_view(), name='clienteventdetail'),
    url(r'^admission/$', ClientAdmissionView.as_view(), name='clientadmission'),
    url(r'^admissionn/$', ClientAdmissionnView.as_view(), name='clientadmissionn'),
    url(r'^feestructure/$', ClientFeestructureView.as_view(), name='clientfeestructure'),
    url(r'^download/$', ClientDownloadListView.as_view(), name='ClientdownloadList'),
    url(r'^routine/$', ClientRoutineListView.as_view(), name='Clientroutinelist'),
    url(r'^result/$', ClientResultListView.as_view(), name='Clientresultlist'),
    url(r'^vacancy/$', ClientVacancyListView.as_view(), name='clientvacancylist'),
    url(r'^search/$', ClientSearchView.as_view(), name='clientsearch'),
    url(r'^me/$', ClientRuppeshView.as_view(), name='clientruppesh'),
    url(r'^thankum/$', ClientThankumView.as_view(), name='clientthankum'),
    url(r'^thankua/$', ClientThankuaView.as_view(), name='clientthankua'),
    url(r'^thankuv/$', ClientThankuvView.as_view(), name='clientthankuv'),


]