from django import forms
from .models import *



class DateTimeInput(forms.DateInput):
    input_type = 'time'


class AdminLoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'text'}))
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'text'}))
            # attrs={'class': 'form-control'}))




class AdminVideoCreateForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ['title', 'url']

    def __init__(self, *args, **kwargs):
        super(AdminVideoCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminVideoDeleteForm(forms.ModelForm):

    class Meta:
        model = Video
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdminBlogCreateForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ['title', 'image', 'content', 'author']

    def __init__(self, *args, **kwargs):
        super(AdminBlogCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminBlogDeleteForm(forms.ModelForm):

    class Meta:
        model = Blog
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminEventCreateForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = ['title', 'image', 'date', 'location', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminEventCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminEventDeleteForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminGalleryCreateForm(forms.ModelForm):
    class Meta:
        model = Gallery
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminGalleryCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminGalleryDeleteForm(forms.ModelForm):

    class Meta:
        model = Gallery
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminNoticeCreateForm(forms.ModelForm):
    class Meta:
        model = Notice
        fields = ['title', 'date', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminNoticeCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminNoticeDeleteForm(forms.ModelForm):

    class Meta:
        model = Notice
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdminStaffCreateForm(forms.ModelForm):
    class Meta:
        model = Staff
        fields = ['name', 'image', 'post', 'phone1', 'email', 'subject']
        
    def __init__(self, *args, **kwargs):
        super(AdminStaffCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminStaffDeleteForm(forms.ModelForm):

    class Meta:
        model = Staff
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminTestimonialCreateForm(forms.ModelForm):
    class Meta:
        model = Testimonial
        fields = ['name', 'post', 'says']

    def __init__(self, *args, **kwargs):
        super(AdminTestimonialCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminTestimonialDeleteForm(forms.ModelForm):

    class Meta:
        model = Testimonial
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }

class AdminActivitiesCreateForm(forms.ModelForm):
    class Meta:
        model = Activities
        fields = ['title', 'content', 'image', 'date', 'location']

    def __init__(self, *args, **kwargs):
        super(AdminActivitiesCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminActivitiesDeleteForm(forms.ModelForm):

    class Meta:
        model = Activities
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }

class AdminFeaturesCreateForm(forms.ModelForm):
    class Meta:
        model = Features
        fields = ['title', 'image', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminFeaturesCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminFeaturesDeleteForm(forms.ModelForm):

    class Meta:
        model = Features
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminServiceCreateForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = ['title', 'image', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminServiceCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminServicesDeleteForm(forms.ModelForm):

    class Meta:
        model = Service
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdminDownloadCreateForm(forms.ModelForm):
    class Meta:
        model = Download
        fields = ['title', 'document', 'image', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminDownloadCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminDownloadDeleteForm(forms.ModelForm):

    class Meta:
        model = Download
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdminRoutineCreateForm(forms.ModelForm):
    class Meta:
        model = Routine
        fields = ['grade', 'document']

    def __init__(self, *args, **kwargs):
        super(AdminRoutineCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminRoutineDeleteForm(forms.ModelForm):

    class Meta:
        model = Routine
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }





class AdminResultCreateForm(forms.ModelForm):
    class Meta:
        model = Result
        fields = ['grade', 'document']

    def __init__(self, *args, **kwargs):
        super(AdminResultCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminResultDeleteForm(forms.ModelForm):

    class Meta:
        model = Result
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdminVacanciiCreateForm(forms.ModelForm):
    class Meta:
        model = Vacancii
        fields = ['title', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminVacanciiCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminVacanciiDeleteForm(forms.ModelForm):

    class Meta:
        model = Vacancii
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdminPriceCreateForm(forms.ModelForm):
    class Meta:
        model = Price
        fields = ['title', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminPriceCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminPriceDeleteForm(forms.ModelForm):

    class Meta:
        model = Price
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }





class AdminSliderCreateForm(forms.ModelForm):
    class Meta:
        model = Slider
        fields = ['image', 'caption']

    def __init__(self, *args, **kwargs):
        super(AdminSliderCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminSliderDeleteForm(forms.ModelForm):

    class Meta:
        model = Slider
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }








class MessageForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Name.'
            }))
    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Phone Number.'
            }))
    subject = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Subject.'
            }))
    email = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Email.'
            }))
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={'class': 'form-control', 'placeholder': 'message.'
            }))
    class Meta:
        model = Message
        fields = ['name', 'phone', 'email', 'subject', 'message']

    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminMessageDeleteForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdmissionForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Full Name.'
            }))
    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Phone Number.'
            }))
    grade = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Select your class.'
            }))
    email = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Email.'
            }))
    percentage = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Previous Class Percentage.'
            }))
    birthdate = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Date of Birth.'
            }))
    address = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Adress.'
            }))
    muncipilaty = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Muncipality.'
            }))
    district = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'District.'
            }))
    zipcode = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Zipcode.'
            }))
    class Meta:
        model = Admission
        fields = ['name', 'phone', 'email', 'birthdate', 'grade', 'percentage', 'address',
         'muncipilaty', 'district', 'zipcode', 'Applicant_PP_size_Photo', 'Applicant_Marksheet_Photo']

    def __init__(self, *args, **kwargs):
        super(AdmissionForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})

class AdminAdmissionDeleteForm(forms.ModelForm):

    class Meta:
        model = Admission
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class VacancyForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Full Name.'
            }))
    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Phone Number.'
            }))
    Qualification = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Qualification'
            }))
    email = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Email.'
            }))
    subject = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Which Subject you want to teach.'
            }))
    percentage = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Previous Qualification Percentage.'
            }))
    birthdate = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Date of Birth.'
            }))
    address = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Adress.'
            }))
    muncipilaty = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Muncipality.'
            }))
    district = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'District.'
            }))
    zipcode = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Zipcode.'
            }))
   

    class Meta:
        model = Vacancy
        fields = ['name', 'phone', 'email', 'birthdate', 'Qualification', 'percentage', 'address', 'subject', 
         'muncipilaty', 'district', 'zipcode', 'Applicant_PP_size_Photo', 'Applicant_Marksheet_Photo']

    def __init__(self, *args, **kwargs):
        super(VacancyForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})

class AdminVacancyDeleteForm(forms.ModelForm):

    class Meta:
        model = Vacancy
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminA1CreateForm(forms.ModelForm):
    class Meta:
        model = A1
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminA1CreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminA1DeleteForm(forms.ModelForm):

    class Meta:
        model = A1
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }

class AdminA2CreateForm(forms.ModelForm):
    class Meta:
        model = A2
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminA2CreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminA2DeleteForm(forms.ModelForm):

    class Meta:
        model = A2
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }

class AdminA3CreateForm(forms.ModelForm):
    class Meta:
        model = A3
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminA3CreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminA3DeleteForm(forms.ModelForm):

    class Meta:
        model = A3
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminA4CreateForm(forms.ModelForm):
    class Meta:
        model = A4
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminA4CreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminA4DeleteForm(forms.ModelForm):

    class Meta:
        model = A4
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }

class AdminA5CreateForm(forms.ModelForm):
    class Meta:
        model = A5
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminA5CreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminA5DeleteForm(forms.ModelForm):

    class Meta:
        model = A5
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminA6CreateForm(forms.ModelForm):
    class Meta:
        model = A6
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminA6CreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminA6DeleteForm(forms.ModelForm):

    class Meta:
        model = A6
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminA7CreateForm(forms.ModelForm):
    class Meta:
        model = A7
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminA7CreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminA7DeleteForm(forms.ModelForm):

    class Meta:
        model = A7
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }

class AdminA8CreateForm(forms.ModelForm):
    class Meta:
        model = A8
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminA8CreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminA8DeleteForm(forms.ModelForm):

    class Meta:
        model = A8
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }