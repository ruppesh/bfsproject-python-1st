# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-13 03:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bfsapp', '0021_auto_20180112_1701'),
    ]

    operations = [
        migrations.AddField(
            model_name='admission',
            name='photos',
            field=models.ImageField(blank=True, null=True, upload_to='admission/'),
        ),
    ]
