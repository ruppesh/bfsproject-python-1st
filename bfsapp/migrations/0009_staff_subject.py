# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-01 15:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bfsapp', '0008_slider'),
    ]

    operations = [
        migrations.AddField(
            model_name='staff',
            name='subject',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
