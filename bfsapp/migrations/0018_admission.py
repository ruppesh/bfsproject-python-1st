# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-11 15:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bfsapp', '0017_auto_20180105_1707'),
    ]

    operations = [
        migrations.CreateModel(
            name='Admission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('name', models.CharField(max_length=255)),
                ('birthdate', models.PositiveIntegerField()),
                ('email', models.EmailField(max_length=254)),
                ('phone', models.PositiveIntegerField(blank=True, null=True)),
                ('grade', models.PositiveIntegerField()),
                ('percentage', models.CharField(max_length=20)),
                ('address', models.CharField(max_length=100)),
                ('muncipilaty', models.CharField(max_length=100)),
                ('district', models.CharField(max_length=100)),
                ('zipcode', models.PositiveIntegerField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
